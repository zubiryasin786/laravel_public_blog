<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TestRoute extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return response()->json([
            'status' => 1,
            'message' => 'Success',
            'route' => route('test'),
            'time' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
